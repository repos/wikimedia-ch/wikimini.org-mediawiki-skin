/**
 * Wikimini-specific scripts
 * 2022.05.14 Rififi : fix lastTabIndex not being loaded and so making eveyrthing crashes
 */
mw.loader.using(['jquery.tabIndex'], function(){
jQuery( function ( $ ) {
	/**
	 * Focus search input at the very end
 	 */
	$( '#searchInput' ).attr( 'tabindex', $( document ).lastTabIndex() + 1 );
	/**
	 * Load iframe when first becomes visible
	 */
	$('#uploadBtn').click(function(){

	    $('#stockIframe').attr('src',$('#stockIframe').data('src'));
		
	    $('#uploadModal').modal({show:true})
	});

	/**
	 * Hide classify link on all but namespace 0
	 */
	if(mw.config.values.wgNamespaceNumber === 0){
		$('#classify').show();
	} else {
		$('#classify').hide();
	}
	/**
	 * Hide modify links in talk namespaces
	 */
	var talkNamespaces = [];
	for(i = 1; i < 14; i+=2){
		talkNamespaces.push(i);
	}
	if(jQuery.inArray(mw.config.values.wgNamespaceNumber, talkNamespaces) !== -1){
		$('li.ca-edit').hide();
		$('li.ca-talk').hide();
	} else {
		$('li.ca-edit').show();
		/* LJ (2017-03-01): Je commente les 5 lignes qui suivent, car je ne veux pas masquer ces liens!)
		$('li.ca-nstab-main').hide();
		$('li.ca-nstab-user').hide();
		if(mw.config.values.wgAction === "history"){
			$('li.ca-nstab-main').show();
			$('li.ca-nstab-user').hide();
		}*/
	}

	/**
	 * Dropdown menu accessibility
	 */
	$( 'div.wikiminiMenu' ).each( function () {
		var $el = $( this );
		$el.find( '> h3 > a' ).parent()
			.attr( 'tabindex', '0' )
			// For accessibility, show the menu when the h3 is clicked (bug 24298/46486)
			.on( 'click keypress', function ( e ) {
				if ( e.type === 'click' || e.which === 13 ) {
					$el.toggleClass( 'menuForceShow' );
					e.preventDefault();
				}
			} )
			// When the heading has focus, also set a class that will change the arrow icon
			.focus( function () {
				$el.find( '> a' ).addClass( 'wikiminiMenuFocus' );
			} )
			.blur( function () {
				$el.find( '> a' ).removeClass( 'wikiminiMenuFocus' );
			} )
			.find( '> a:first' )
			// As the h3 can already be focused there's no need for the link to be focusable
			.attr( 'tabindex', '-1' );
	} );

	/**
	 * Collapsible tabs
	 */
	var $cactions = $( '#p-cactions' ),
		$tabContainer = $( '#p-views ul' ),
		originalDropdownWidth = $cactions.width();

	// Bind callback functions to animate our drop down menu in and out
	// and then call the collapsibleTabs function on the menu
	$tabContainer
		.bind( 'beforeTabCollapse', function () {
			// If the dropdown was hidden, show it
			if ( $cactions.hasClass( 'emptyPortlet' ) ) {
				$cactions
					.removeClass( 'emptyPortlet' )
					.find( 'h3' )
						.css( 'width', '1px' ).animate( { width: originalDropdownWidth }, 'normal' );
			}
		} )
		.bind( 'beforeTabExpand', function () {
			// If we're removing the last child node right now, hide the dropdown
			if ( $cactions.find( 'li' ).length === 1 ) {
				$cactions.find( 'h3' ).animate( { width: '1px' }, 'normal', function () {
					$( this ).attr( 'style', '' )
						.parent().addClass( 'emptyPortlet' );
				} );
			}
		} )
		.collapsibleTabs( {
			expandCondition: function ( eleWidth ) {
				// (This looks a bit awkward because we're doing expensive queries as late as possible.)

				var distance = $.collapsibleTabs.calculateTabDistance();
				// If there are at least eleWidth + 1 pixels of free space, expand.
				// We add 1 because .width() will truncate fractional values but .offset() will not.
				if ( distance >= eleWidth + 1 ) {
					return true;
				} else {
					// Maybe we can still expand? Account for the width of the "Actions" dropdown if the
					// expansion would hide it.
					if ( $cactions.find( 'li' ).length === 1 ) {
						return distance >= eleWidth + 1 - originalDropdownWidth;
					} else {
						return false;
					}
				}
			},
			collapseCondition: function () {
				// (This looks a bit awkward because we're doing expensive queries as late as possible.)
				// TODO The dropdown itself should probably "fold" to just the down-arrow (hiding the text)
				// if it can't fit on the line?

				// If there's an overlap, collapse.
				if ( $.collapsibleTabs.calculateTabDistance() < 0 ) {
					// But only if the width of the tab to collapse is smaller than the width of the dropdown
					// we would have to insert. An example language where this happens is Lithuanian (lt).
					if ( $cactions.hasClass( 'emptyPortlet' ) ) {
						return $tabContainer.children( 'li.collapsible:last' ).width() > originalDropdownWidth;
					} else {
						return true;
					}
				} else {
					return false;
				}
			}
		} );
} ); //jQuery
}); // mw.loader


///////////////////////////////// Feuille de style Wikimini

//20161229/Nixit
//importStylesheet('MediaWiki:MenuOptions.css');
// 2022.05.14 Rififi import is not valid, ?action= not & (& is only if you use /w/index.php?title=title). Page is deleted on wiki anyway, so removing.
// mw.loader.load('/wiki/MediaWiki:MenuOptions.css&action=raw&ctype=text/css','text/css');
//


var now = new Date();
var nextYear = new Date(now.getTime() + 1000 * 60 * 60 * 24 * 365 );
var headerFlashStyle = true;
var swiffyStageLoaded = false;
var hiddenMenuIcon="show-header-b.png";
var shownMenuIcon="hide-header-b.png";


var fullSizeIcon="expand-b.png";
var normalSizeIcon="shrink-b.png";


if ($("#HeaderButton").length ){
	var imgPath= $("#HeaderButton img").attr('src').split('/')
	imgPath.pop();
	imgPath= imgPath.join('/')+'/';
	var headerState = getCookie("HeaderState");
	if(headerState != "0"){
		headerFlashStyle = true;
	}else{
		headerFlashStyle = false;
		hideFlashMenu();
	}
	var fullSize = parseInt(getCookie("fullSize"));
	if (fullSize){
		setContentFullSize();
	}else{
		setDefaultSize();
	}
	var bodyDefaultFontSize = 14;
	var bodyFontSize = parseInt(getCookie("bodyFontSize"));
	var expandedContent= parseInt(getCookie("expandedContent"));
	if (bodyFontSize < 4)
		bodyFontSize = bodyDefaultFontSize;
	if (bodyFontSize > 40)
		bodyFontSize = 40;
	if(bodyFontSize != bodyDefaultFontSize){
		$('#bodyContent').css('font-size', bodyFontSize);
	}
	
	/*** Edit mode - categories ****/

	if ($(".categoriesTitle").length ){
		$( ".categoriesTitle" ).click(function() {
			  $( "#SelectCategoryList" ).toggle( "fast", function() {
				  if ($(".categoriesTitle > div.reversed").length){
					  $(".categoriesTitle > div").removeClass('reversed');
				  }else{
					  $(".categoriesTitle > div").addClass('reversed');
				  }
			  });
			  
			});
	}
	
}

// 2022.05.15 Rififi: fixing swiffy not being loaded at time
mw.loader.using('swiffy.fr.js', function() {
$( document ).ready(function() {
	
	
	
	if (headerFlashStyle){
		
		var stage = new swiffy.Stage(document.getElementById('swiffycontainer'),swiffyobject, {  });
			
		stage.setFlashVars(flashButton);
		stage.start();
		swiffyStageLoaded = true;
	}
	$('#HeaderButton').click(function() {
			toggleMenu();
	});
	$('#SiteWidth').click(function() {
			toggleWidth();
	});
	$('#IncreaseFont').click(function() {
		increaseFont();
	});

	$('#DecreaseFont').click(function() {
		decreaseFont();
	});

	$('#DefaultFontSize').click(function() {
		defaultFontSize();
	})
	
	$('#PrintVersion').click(function() {
		printPage();
	})
	inlineIndexTable();

	
	$('[data-toggle="tooltip"]').tooltip();
	
	
	/***prefererences *****/
	if ($('#preferences').length){
		initPreferences();
	}
	
	/*** modifications recentes ***/
	if ($('.rcshowhide').length){
		$('.rcshowhide').html($('.rcshowhide').html().split("|").join(""));
		$('table.mw-recentchanges-table tbody>tr:nth-child(1) td:nth-child(2)').append(
			$('table.mw-recentchanges-table tbody>tr:nth-child(2) td:nth-child(2) input:nth-child(2) ').clone().wrap('<div>').parent().html());
			$('table.mw-recentchanges-table tbody>tr:nth-child(2)').remove();
	}

	var classify = getUrlVars()["classify"];
	if (classify !== undefined){
		$('#editform #toolbar').hide();
		$('#wpTextbox1').hide();

		setTimeout( function(){$('.wikiEditor-ui').hide();$('#wpTextbox1').hide();}, 200);
		setTimeout( function(){$('.wikiEditor-ui').hide();$('#wpTextbox1').hide();}, 600);
		setTimeout( function(){$('.wikiEditor-ui').hide();$('#wpTextbox1').hide();}, 1000);
	}

	
});// jQuery
});// mw.loader.using

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

/*** vedit ***/
/**mw.hook( 've.activationComplete' ).add(function(){
	$( document ).ready(function() {
		if ($('.oo-ui-toolbar-tools').length){
			console.log("coucou");
			$('.oo-ui-toolbar-tools > div').insertAfter( '<div class="responsive-clear"> </div>' );
				
		}
	});
});**/



