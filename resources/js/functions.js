
// ==================================================================================================
//////////// FONCTIONS RELATIVES AUX COOKIES //////////////
function setCookie (cookieName, cookieValue, expires, path) {
        document.cookie = escape(cookieName) + '=' + escape(cookieValue) + (expires ? '; EXPIRES=' + expires.toGMTString() : '') + "; PATH=/" ; 
}
  
function getCookie(cookieName) {
        var cookieValue = null;
        var posName = document.cookie.indexOf(escape(cookieName) + '=' );
        if (posName != -1) {
                        var posValue = posName + (escape(cookieName) + '=' ).length;
                        var endPos = document.cookie.indexOf(';', posValue) ;
                if (endPos != -1) {
                        cookieValue = unescape(document.cookie.substring(posValue, endPos));
                } else {
                        cookieValue = unescape(document.cookie.substring(posValue));
                }
        }
        return decodeURIComponent(cookieValue);
}
// ============================================================

function hideFlashMenu(){
	headerFlashStyle = false;
	$("#HeaderButton img").attr('src',imgPath+hiddenMenuIcon);
	$("#HeaderButton img").attr('alt',$("#HeaderButton").attr('data-text2'));
	$("#HeaderButton").attr('data-original-title',$("#HeaderButton").attr('data-text2'));
    setCookie("HeaderState", "0", nextYear);
	$('#swiffycontainer').addClass('div-hidden');
	$('#mobileNav').addClass('div-visible');
}

function showFlashMenu(){
	headerFlashStyle = true;
	$("#HeaderButton").attr('data-original-title',$("#HeaderButton").attr('data-text1'));
	$("#HeaderButton img").attr('src',imgPath+shownMenuIcon);
	$("#HeaderButton img").attr('alt',$("#HeaderButton").attr('data-text1'))
    setCookie("HeaderState", "1", nextYear);
	$('#swiffycontainer').removeClass('div-hidden');
	$('#mobileNav').removeClass('div-visible');
	if (!swiffyStageLoaded){
		// 2022.05.15 Rififi: fix swiffy not being properly loaded
		mw.loader.using('swiffy.fr.js', function() {
			var stage = new swiffy.Stage(document.getElementById('swiffycontainer'),swiffyobject, {  });	
	
			stage.setFlashVars(flashButton);
			stage.start();
			swiffyStageLoaded =true;
		});
	}

}

function setContentFullSize(){
	fullSize=1;
    setCookie("fullSize", "1", nextYear);
	$("#SiteWidth img").attr('src',imgPath+normalSizeIcon);
	$("#SiteWidth img").attr('alt',$("#SiteWidth").attr('data-text2'));
	$("#SiteWidth").attr('data-original-title',$("#SiteWidth").attr('data-text2'));
	
	$('.mw-body').addClass("mw-body-fluid");
	$('.fluid-candidate').addClass("fluid-row");
	$('#fluid-content').addClass("fluid-content");
	$('#fluid-sidebar').addClass("fluid-sidebar");
}

function setDefaultSize(){
	fullSize=0;
    setCookie("fullSize", "0", nextYear);
	$("#SiteWidth img").attr('src',imgPath+fullSizeIcon);
	$("#SiteWidth img").attr('alt',$("#SiteWidth").attr('data-text1'));
	$("#SiteWidth").attr('data-original-title',$("#SiteWidth").attr('data-text1'));
	
	$('.mw-body').removeClass("mw-body-fluid");
	$('.fluid-candidate').removeClass("fluid-row");
	$('#fluid-content').removeClass("fluid-content");
	$('#fluid-sidebar').removeClass("fluid-sidebar");

	
}

function toggleMenu(){
	if (headerFlashStyle){
		hideFlashMenu();
	}else{
		showFlashMenu();
	}
	$('#HeaderButton').tooltip('hide');
}

function toggleWidth(){
	if (fullSize !=0){		
		setDefaultSize();
	}else{
		setContentFullSize();
	}
	$('#SiteWidth').tooltip('hide');
}

function increaseFont(){
	if (bodyFontSize > 40)
		return;
	bodyFontSize+=2;
	$('#bodyContent').css('font-size', bodyFontSize);
    setCookie("bodyFontSize", bodyFontSize, nextYear);
	
}

function decreaseFont(){

	if (bodyDefaultFontSize < 6)
		return;
	bodyFontSize-=2;
	$('#bodyContent').css('font-size', bodyFontSize);
    setCookie("bodyFontSize", bodyFontSize, nextYear);
	
}

function defaultFontSize(){
	bodyFontSize = bodyDefaultFontSize;
	$('#bodyContent').css('font-size', bodyFontSize);
    setCookie("bodyFontSize", bodyFontSize, nextYear);
	
}

function printPage(){
 window.open(wgServer + wgScript + '?title=' + mw.config.get( 'wgPageName' ) + '&printable=yes', '_self');
}

function inlineIndexTable(){
	/** table to ul & li **/
	  if ($('.mw-prefixindex-list-table').length > 0){
		 var indexTable = $('.mw-prefixindex-list-table');		 
		 indexTable.after( '<ul class="mw-index-ul"> </ul>');
		 var indexUL = $('.mw-index-ul');
		 $('.mw-prefixindex-list-table td').each(function() {
			 indexUL.append( '<li>'+ $(this).html()+'</li>' );
		 });
		 indexTable.remove();		 
	  }
	  /** page navigation header **/
	  if ($('#mw-prefixindex-nav-table #mw-prefixindex-nav-form').length > 0){
		  var indexNav = $('#mw-prefixindex-nav-form').html();
		  $('#mw-prefixindex-nav-form').remove();
		  $('#mw-prefixindex-nav-table > tbody > tr:last-child').after('<tr><td id="mw-prefixindex-nav-form" class="mw-prefixindex-nav">'+indexNav+"</td></tr>")
	  }
}


function initPreferences(){
	var tabsToShow = ['mw-prefsection-personal','mw-prefsection-rendering'];
	var fieldsetToHide = ['mw-prefsection-personal-i18n','mw-prefsection-rendering-skin','mw-prefsection-rendering-files', 'mw-prefsection-rendering-diffs', 'mw-prefsection-rendering-advancedrendering' ]
	var isfirst = true;
	$('#mw-input-wpnickname').attr('size','20');
	$('#mw-input-wprealname').attr('size','20');
	var key;
	for (key in fieldsetToHide){
		$('#'+key).hide();
	}
	$('#mw-input-wplanguage').prop('disabled','disabled');
	$('#preferences').prepend('<ul class="nav nav-tabs" role="tablist" id="preferencesList"> </ul>');
	 $('#preferences > fieldset').each(function() {
		 if (tabsToShow.indexOf($(this).attr('id')) ==-1){
			 $(this).addClass('tab-pane');			 
		 }else{
			 $(this).prepend('<h2>'+$('#'+$(this).attr('id')+' > legend').html()+'</h2>');
			 $('#'+$(this).attr('id')+' > legend').remove();
			 $(this).attr('role','tabpanel');
			 $(this).addClass('tab-pane');
			 if (isfirst){
				 isfirst = false;
				 $('#preferencesList').append('<li role="presentation" class="active"><a href="#'+$(this).attr('id')+'" aria-controls="'+$(this).attr('id')+'" role="tab" data-toggle="tab">'+$(this).find('legend').html()+'</a></li>')
				 $(this).addClass('active');
			 }else{	 
				 $('#preferencesList').append('<li role="presentation"><a href="#'+$(this).attr('id')+'" aria-controls="'+$(this).attr('id')+'" role="tab" data-toggle="tab">'+$(this).find('legend').html()+'</a></li>')
			 }
		 }
	 });
}

String.prototype.replaceAll = function(search, replace) {
    if (replace === undefined) {
        return this.toString();
    }
    return this.split(search).join(replace);
}

function getArticleLink(){

	var splitHref = $('.ca-talk a').attr('href').split(':');

	return splitHref[0].substring(0,19) + splitHref[1];
}

function getProfileLink(){

	var splitHref = $('.ca-talk a').attr('href').split(':');

	return "/wiki/Utilisateur:" + splitHref[1];
}
