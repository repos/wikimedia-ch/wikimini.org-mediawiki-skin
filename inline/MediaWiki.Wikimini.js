// ============================================================
/* tooltips and access keys */
ta = new Object();
ta['pt-userpage'] = new Array('.','Ta page personnelle où tu peux te présenter');
ta['pt-anonuserpage'] = new Array('.','La page utilisateur pour l’adresse IP que tu utilises');
ta['pt-mytalk'] = new Array('n','Ta page de discussion personnelle');
ta['pt-anontalk'] = new Array('n','La page de discussion de cette adresse IP');
ta['pt-preferences'] = new Array('','Tes préférences');
ta['pt-watchlist'] = new Array('l','La liste des pages que tu suis ou "surveilles"');
ta['pt-mycontris'] = new Array('y','La liste de tes contributions');
ta['pt-login'] = new Array('o','Tu dois te connecter pour participer à Wikimini');
ta['pt-anonlogin'] = new Array('o','Tu dois te connecter pour participer à Wikimini');
ta['pt-logout'] = new Array('o','Se déconnecter');
ta['ca-talk'] = new Array('t','Discussions sur cette page');
ta['ca-edit'] = new Array('e','Tu peux modifier cette page. Merci de prévisualiser avant de publier');
ta['ca-addsection'] = new Array('+','Ajoute un commentaire à cette discussion');
ta['ca-viewsource'] = new Array('e','Cette page est protégée. Tu peux voir son code source.');
ta['ca-history'] = new Array('h','Version précédentes de cette page');
ta['ca-protect'] = new Array('=','Protège cette page');
ta['ca-delete'] = new Array('d','Supprime cette page');
ta['ca-undelete'] = new Array('d','Restaure les modifications faites à cette page avant sa suppression');
ta['ca-move'] = new Array('m','Déplace cette page');
ta['ca-watch'] = new Array('w','Ajoute cette page à la liste des pages que tu surveilles');
ta['ca-unwatch'] = new Array('w','Retire cette page de la liste des articles que tu surveilles');
ta['search'] = new Array('f','Cherche un article dans Wikimini');
ta['p-logo'] = new Array('','Page principale');
ta['n-mainpage'] = new Array('z','Retourne à la page principale');
ta['n-portal'] = new Array('','À propos du projet, ce que tu peux faire, où trouver des renseignements');
ta['n-currentevents'] = new Array('','Trouve des informations sur les événements récents');
ta['n-recentchanges'] = new Array('r','La liste des dernières modifications effectuées sur Wikimini.');
ta['n-randompage'] = new Array('x','Visite une page de Wikimini au hasard');
ta['n-help'] = new Array('','Aide');
ta['n-sitesupport'] = new Array('','Aidez-nous');
ta['t-whatlinkshere'] = new Array('j','Liste des pages qui ont un lien pointant vers celle-ci');
ta['t-recentchangeslinked'] = new Array('k','Liste des modifications récentes sur les pages liées à celle-ci');
ta['feed-rss'] = new Array('','Flux RSS de cette page');
ta['feed-atom'] = new Array('','Flux Atom de cette page');
ta['t-contributions'] = new Array('','Regarde la liste des contributions de ce Wikiminaute');
ta['t-emailuser'] = new Array('','Envoie un courriel (e-mail) à ce Wikiminaute');
ta['t-upload'] = new Array('u','Ajoute une image dans le stock de Wikimini');
ta['t-specialpages'] = new Array('q','Liste des pages spéciales de Wikimini');
ta['ca-nstab-main'] = new Array('c','Affiche le contenu de cette page');
ta['ca-nstab-user'] = new Array('c','Affiche la page personnelle de ce Wikiminaute');
ta['ca-nstab-media'] = new Array('c','Affiche la page média');
ta['ca-nstab-special'] = new Array('','Tu es sur une page spéciale. Tu ne peux pas la modifier.');
ta['ca-nstab-wp'] = new Array('a','Affiche la page du projet');
ta['ca-nstab-image'] = new Array('c','Affiche la page de l’image');
ta['ca-nstab-mediawiki'] = new Array('c','Affiche le message système');
ta['ca-nstab-template'] = new Array('c','Affiche le modèle');
ta['ca-nstab-help'] = new Array('c','Affiche la page d’aide');
ta['ca-nstab-category'] = new Array('c','Affiche la page de catégorie');

// ============================================================
// BEGIN Enable multiple onload functions

// setup onload functions this way:
// aOnloadFunctions[aOnloadFunctions.length] = function_name; // without brackets!

if (!window.aOnloadFunctions) {
  var aOnloadFunctions = new Array();
}

window.onload = function() {
  if (window.aOnloadFunctions) {
    for (var _i=0; _i<aOnloadFunctions.length; _i++) {
      aOnloadFunctions[_i]();
    }
  }
}

// END Enable multiple onload functions
// ============================================================

// ============================================================
if(wgNamespaceNumber==6){ //////////// ESPACE DE NOMS  "FICHIER" //////////////

// Dans la page de description d'une image, ajoute un texte d'aide pour insérer une image
//20161229/Nixit
//addOnloadHook(function(){
jQuery( document ).ready( function( $ ) {
//
     var SectionHistory = document.getElementById('filehistory');
     if(!SectionHistory) return;
 
     var MessageAide = document.createElement('div');
     MessageAide.className="fck_mw_template";
     MessageAide.innerHTML = '<table cellspacing="10" cellpadding="1" width="100%" align="center" style="background-color: #F7F7F7; border: solid 1px #DDDDDD;">'
                            +'<tr>'
                            +'<td valign="top" width="45" > <img height="44" border="0" width="30" src="https://stock.wikimini.org/w/images/e/e9/Bg-pin.gif" alt=""/>'
                            +'</td>'
                            +'<td style="text-align:justify;" > Pour insérer cette image dans un article, clique sur l\'icône <img height="21" border="0" width="21" src="https://stock.wikimini.org/w/images/e/e7/Fckeditor-Image.gif" alt="Image:Fckeditor-Image.gif"/> de la barre d\'outils et cherche l\'image nommée <b>\«&nbsp;' + wgTitle + '&nbsp;\»</b>.'
                            +'</td>'
                            +'</table>';
     SectionHistory.parentNode.insertBefore(MessageAide, SectionHistory);  
 
});

} //////////// FIN DE L'ESPACE DE NOMS  "FICHIER" //////////////
// ============================================================

// =============================================== BOT : recherche de page sans images
if(wgUserGroups!=null){
        if(wgUserGroups.indexOf('sysop')!=-1){    
                if(wgPageName=="Wikimini:Pages_à_illustrer"){           
                        importScript('MediaWiki:Bot/PagesSansImage.js');
                }
        }
}
// ===============================================

// ============================================================================
///////////////////////////////// Évite le double-post dans les pages de discussion LQT
//20161229/Nixit
//addOnloadHook(function(){
jQuery( document ).ready( function( $ ) {
//
    var wpSave = document.getElementById('wpSave');
    var wpForm = document.getElementById('editform');
    if((wpSave)&&(wpForm)){
        wpSave.onclick = function(){
            document.getElementById('editform').submit();
            document.getElementById('wpSave').disabled = "disabled";            
        }
    }
});///////////////////////////////
// ===========================================================================


///////////////////////////////// Menu options ergonomiques
if(wgUserName && wgUserName == "Dr Brains"){
  importScript('MediaWiki:MenuOptions_Dev.js');
}else{
  importScript('frim');
}
// ===========================================================================

///////////////////////////////// Fonctions Javascript liées à des modèles
importScript('MediaWiki:Modeles.js');
// ===========================================================================

///////////////////////////////// Feuille de style Wikimini

//20161229/Nixit
//importStylesheet('MediaWiki:Wikimini.css');
//mw.loader.load('/w/index.php?title=MediaWiki:Wikimini.css');
//mw.loader.load('/wiki/MediaWiki:Wikimini.css');
importStylesheetURI('/w/MediaWiki:Wikimini.css');
//

// ===========================================================================

///////////////////////////////// Patch forum, en attendant solution PHP 


//20161229/Nixit
//addOnloadHook(AWCForum_Patch);
jQuery( document ).ready( AWCForum_Patch ) {
//

function AWCForum_Patch(){
    var AllDivs = document.getElementsByTagName('div');
    var Count = 0
    for(var a=0;a<AllDivs.length;a++){
        var ID = AllDivs[a].id;
        if(ID){
            if(ID == "cat_list_lastaction_title"){
                Count++
                var Link = AllDivs[a].getElementsByTagName('a')[0];
                var Target = DecodeHTMLEntities(decodeURIComponent(Link.href));
                Link.href = Target;
                var Text = Target.replace(/.*id[^\/]+\//, "").replace(/\.html/, "");
                Link.title = Text;
                var CuttedText = Text.substring(0, 20);
                if(Text!=CuttedText) CuttedText += "...";
                Link.innerHTML = CuttedText;
            }
        }
    }
    //alert(Count);
}

function DecodeHTMLEntities(text){
    var EncodedCharacter = new Array();
    var DecodedCharacter = new Array();
    var CharacterCount = 0;
 
    EncodedCharacter[CharacterCount] = "&amp;";
    DecodedCharacter[CharacterCount] = "&";
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&#039;";
    DecodedCharacter[CharacterCount] = "'";
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&quot;";
    DecodedCharacter[CharacterCount] = '"';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&laquo;";
    DecodedCharacter[CharacterCount] = '«';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&raquo;";
    DecodedCharacter[CharacterCount] = '»';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&rsquo;";
    DecodedCharacter[CharacterCount] = '’';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&hellip;";
    DecodedCharacter[CharacterCount] = '…';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&mdash;";
    DecodedCharacter[CharacterCount] = '—';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&ndash;";
    DecodedCharacter[CharacterCount] = '–';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&bull;";
    DecodedCharacter[CharacterCount] = '•';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&nbsp;";
    DecodedCharacter[CharacterCount] = ' ';
    CharacterCount++;

    EncodedCharacter[CharacterCount] = "&Agrave;";
    DecodedCharacter[CharacterCount] = 'À';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Aacute;";
    DecodedCharacter[CharacterCount] = 'Á';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Acirc;";
    DecodedCharacter[CharacterCount] = 'Â';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Atilde;";
    DecodedCharacter[CharacterCount] = 'Ã';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Auml;";
    DecodedCharacter[CharacterCount] = 'Ä';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Aring;";
    DecodedCharacter[CharacterCount] = 'Å';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&AElig;";
    DecodedCharacter[CharacterCount] = 'Æ';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Ccedil;";
    DecodedCharacter[CharacterCount] = 'Ç';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Egrave;";
    DecodedCharacter[CharacterCount] = 'È';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Eacute;";
    DecodedCharacter[CharacterCount] = 'É';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Ecirc;";
    DecodedCharacter[CharacterCount] = 'Ê';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Euml;";
    DecodedCharacter[CharacterCount] = 'Ë';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Igrave;";
    DecodedCharacter[CharacterCount] = 'Ì';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Iacute;";
    DecodedCharacter[CharacterCount] = 'Í';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Icirc;";
    DecodedCharacter[CharacterCount] = 'Î';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Iuml;";
    DecodedCharacter[CharacterCount] = 'Ï';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Ntilde;";
    DecodedCharacter[CharacterCount] = 'Ñ';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Ograve;";
    DecodedCharacter[CharacterCount] = 'Ò';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Oacute;";
    DecodedCharacter[CharacterCount] = 'Ó';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Ocirc;";
    DecodedCharacter[CharacterCount] = 'Ô';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Otilde;";
    DecodedCharacter[CharacterCount] = 'Õ';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&OElig;";
    DecodedCharacter[CharacterCount] = 'Œ';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Ouml;";
    DecodedCharacter[CharacterCount] = 'Ö';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Oslash;";
    DecodedCharacter[CharacterCount] = 'Ø';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Ugrave;";
    DecodedCharacter[CharacterCount] = 'Ù';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Uacute;";
    DecodedCharacter[CharacterCount] = 'Ú';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Ucirc;";
    DecodedCharacter[CharacterCount] = 'Û';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&Uuml;";
    DecodedCharacter[CharacterCount] = 'Ü';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&szlig;";
    DecodedCharacter[CharacterCount] = 'ß';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&agrave;";
    DecodedCharacter[CharacterCount] = 'à';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&aacute;";
    DecodedCharacter[CharacterCount] = 'á';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&acirc;";
    DecodedCharacter[CharacterCount] = 'â';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&atilde;";
    DecodedCharacter[CharacterCount] = 'ã';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&auml;";
    DecodedCharacter[CharacterCount] = 'ä';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&aring;";
    DecodedCharacter[CharacterCount] = 'å';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&aelig;";
    DecodedCharacter[CharacterCount] = 'æ';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&ccedil;";
    DecodedCharacter[CharacterCount] = 'ç';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&egrave;";
    DecodedCharacter[CharacterCount] = 'è';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&eacute;";
    DecodedCharacter[CharacterCount] = "é";
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&ecirc;";
    DecodedCharacter[CharacterCount] = 'ê';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&euml;";
    DecodedCharacter[CharacterCount] = 'ë';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&igrave;";
    DecodedCharacter[CharacterCount] = 'ì';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&iacute;";
    DecodedCharacter[CharacterCount] = 'í';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&icirc;";
    DecodedCharacter[CharacterCount] = 'î';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&iuml;";
    DecodedCharacter[CharacterCount] = 'ï';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&ntilde;";
    DecodedCharacter[CharacterCount] = 'ñ';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&ograve;";
    DecodedCharacter[CharacterCount] = 'ò';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&oacute;";
    DecodedCharacter[CharacterCount] = 'ó';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&ocirc;";
    DecodedCharacter[CharacterCount] = 'ô';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&otilde;";
    DecodedCharacter[CharacterCount] = 'õ';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&oelig;";
    DecodedCharacter[CharacterCount] = 'œ';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&ouml;";
    DecodedCharacter[CharacterCount] = 'ö';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&oslash;";
    DecodedCharacter[CharacterCount] = 'ø';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&ugrave;";
    DecodedCharacter[CharacterCount] = 'ù';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&uacute;";
    DecodedCharacter[CharacterCount] = 'ú';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&ucirc;";
    DecodedCharacter[CharacterCount] = 'û';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&uuml;";
    DecodedCharacter[CharacterCount] = 'ü';
    CharacterCount++;
    EncodedCharacter[CharacterCount] = "&yuml;";
    DecodedCharacter[CharacterCount] = 'ÿ';
    CharacterCount++;
 
    for(var a=0;a<EncodedCharacter.length;a++){
        while(text.indexOf(EncodedCharacter[a])!=-1){
            text = text.split(EncodedCharacter[a]).join(DecodedCharacter[a]);
            if(text.indexOf(EncodedCharacter[a])==-1) break;
        }
    }
    return text;
}
// ===========================================================================

// ===========================================================================

///////////////////////////////// Ajout boutons "Tout cocher", "Tout décocher" et "Inverser" dans [[Special:Search]]


if(wgCanonicalSpecialPageName=="Search") 
//20161229/Nixit
	//addOnloadHook(SearchDeluxe);
	jQuery( document ).ready( SearchDeluxe ) {
//


function SearchDeluxe(){
     var Form = document.getElementById("powersearch");
     if(!Form) return;

     var P = Form.getElementsByTagName('p')[0];
     if(!P) return;
     var NewP = document.createElement('p');
     var NewOption = document.createElement('span');
     NewOption.setAttribute('style', 'margin-left:1em;margin-bottom:0.5em;');
     var Content = '<input type="button" onclick="SearchDeluxe_Toggle(1);" onselect="SearchDeluxe_Toggle(1);" title="Tout cocher" value="Tout cocher" />'
                 + '&nbsp;&nbsp;' 
                 + '<input type="button" onclick="SearchDeluxe_Toggle(0);" onselect="SearchDeluxe_Toggle(0);" title="Tout décocher" value="Tout décocher" />'
                 + '&nbsp;&nbsp;'
                 + '<input type="button" onclick="SearchDeluxe_Toggle(-1);" onselect="SearchDeluxe_Toggle(-1)" title="Inverser" value="Inverser" />';
     NewOption.innerHTML = Content;
     NewP.appendChild(NewOption);
     P.parentNode.insertBefore(NewP, P.nextSibling);
}

function SearchDeluxe_Toggle(Mode){
     var Form = document.getElementById("powersearch");
     if(!Form) return;
     var Inputs = Form.getElementsByTagName('input');
     for(var a=0;a<Inputs.length;a++){
          if(Inputs[a].type!="checkbox") continue;
          if(Mode==1){
               Inputs[a].checked = "checked";
          }else if(Mode==0){
               Inputs[a].checked = false;
          }else{
               if(Inputs[a].checked){
                    Inputs[a].checked = false;
               }else{
                    Inputs[a].checked = "checked";
               }
          } 
     }
}
// ===========================================================================

///////////////////////////////// Affichage alerte pour vrais liens externes

//20161229/Nixit
//addOnloadHook(function(){
jQuery( document ).ready( function( $ ) {
//
   var Links = document.getElementsByTagName("a"); 
   for(var a=0,l=Links.length;a<l;a++){
      if(checkLink(Links[a])){
         Links[a].onclick = function(){
            var Confirm = confirm("Attention : Tu vas quitter le site Wikimini FR. Es-tu sûr(e) de vouloir continuer ?");
            return Confirm;
         }
      }
   }


   function checkLink(obj){
      var href = obj.href.toLowerCase();
      var hostname = "wikimini.";
      return ((href.indexOf("http://")!=-1 && href.indexOf(hostname)==-1) ? true : false);              
   }
});
