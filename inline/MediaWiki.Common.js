/* Tout JavaScript ici sera chargé avec chaque page accédée par n’importe quel utilisateur. */

// ============================================================
//////////// FONCTION : .indexOf (Patch IE) //////////////

if (!Array.indexOf) {
  Array.prototype.indexOf = function(obj,idx) {
    var len = this.length;
    if (len == 0) return -1;
    if (!idx) idx = 0;
    while (idx < 0) idx = len + idx;
    for (var i = 0; i < len; i++)
      if (this[i] == obj)
        return i;
    return -1;
  }
}
// ============================================================

// ============================================================
//////////// FONCTION : .HTMLize (évite des attaques XSS) //////////////
if(!String.HTMLize){
  String.prototype.HTMLize = function() {
    var chars = new Array('&','<','>','"');
    var entities = new Array('amp','lt','gt','quot');
    var string = this;
    for (var i=0; i<chars.length; i++) {
      var regex = new RegExp(chars[i], "g");
      string = string.replace(regex, '&' + entities[i] + ';');
    }
    return string;
  }
}
// ============================================================

// ==================================================================================================
//////////// FONCTIONS RELATIVES AUX COOKIES //////////////
function setCookie (cookieName, cookieValue, expires, path) {
        document.cookie = escape(cookieName) + '=' + escape(cookieValue) + (expires ? '; EXPIRES=' + expires.toGMTString() : '') + "; PATH=/" ; 
}
  
function getCookie(cookieName) {
        var cookieValue = null;
        var posName = document.cookie.indexOf(escape(cookieName) + '=' );
        if (posName != -1) {
                        var posValue = posName + (escape(cookieName) + '=' ).length;
                        var endPos = document.cookie.indexOf(';', posValue) ;
                if (endPos != -1) {
                        cookieValue = unescape(document.cookie.substring(posValue, endPos));
                } else {
                        cookieValue = unescape(document.cookie.substring(posValue));
                }
        }
        return decodeURIComponent(cookieValue);
}
// ============================================================

// ============================================================
//////////// FONCTIONS : MANIPULATION DE CLASSES //////////////
function hasClass(node, className) {
  if (node.className == className) {
    return true;
  }
  var reg = new RegExp('(^| )'+ className +'($| )')
  if (reg.test(node.className)) {
    return true;
  }
  return false;
}

function addClass(node, className) {
    if (hasClass(node, className)) {
        return false;
    }
    node.className += ' '+ className;
    return true;
}
 
function removeClass(node, className) {
  if (!hasClass(node, className)) {
    return false;
  }
  node.className = eregReplace('(^|\\s+)'+ className +'($|\\s+)', ' ', node.className);
  return true;
}
 
function eregReplace(search, replace, subject) {
    return subject.replace(new RegExp(search,'g'), replace);
}

// ============================================================
//////////// FONCTION : RECUPERE LE CONTENU TEXTUEL D'UN ELEMENT //////////////
function getTextContent(oNode) {
  if (typeof(oNode.textContent)!="undefined") {return oNode.textContent;}
  switch (oNode.nodeType) {
    case 3: // TEXT_NODE
    case 4: // CDATA_SECTION_NODE
      return oNode.nodeValue;
      break;
    case 7: // PROCESSING_INSTRUCTION_NODE
    case 8: // COMMENT_NODE
      if (getTextContent.caller!=getTextContent) {
        return oNode.nodeValue;
      }
      break;
    case 9: // DOCUMENT_NODE
    case 10: // DOCUMENT_TYPE_NODE
    case 12: // NOTATION_NODE
      return null;
      break;
  }
  var _textContent = "";
  oNode = oNode.firstChild;
  while (oNode) {
    _textContent += getTextContent(oNode);
    oNode = oNode.nextSibling;
  }
  return _textContent;
}
// ===============================================


// ===============================================
//////////// FONCTION : INSERE UN ELEMENT APRES UN ELEMENT DE REFERENCE //////////////
function insertAfter(element, reference) {
    if(reference.nextSibling!=null){
        reference.parentNode.insertBefore(element, reference.nextSibling);
    }else{
        reference.parentNode.appendChild(element);
    }
}
// ===============================================


// ===============================================
//////////// FONCTION : DECODE LE HTML ISSU D'UNE REQUETE API //////////////
function API_HTMLDecode(text){
      var EncodedCharacter = new Array();
      var DecodedCharacter = new Array();
      var CharacterCount = 0;
 
      EncodedCharacter[CharacterCount] = "&amp;";
      DecodedCharacter[CharacterCount] = "&";
      CharacterCount++;
      EncodedCharacter[CharacterCount] = "&#039;";
      DecodedCharacter[CharacterCount] = "'";
      CharacterCount++;
      EncodedCharacter[CharacterCount] = "&quot;";
      DecodedCharacter[CharacterCount] = '"';
      CharacterCount++;
 
      for(var a=0;a<EncodedCharacter.length;a++){
            while(text.indexOf(EncodedCharacter[a])!=-1){
                  text = text.split(EncodedCharacter[a]).join(DecodedCharacter[a]);
                  if(text.indexOf(EncodedCharacter[a])==-1) break;
            }
      }
      return text;
}
// ===============================================


// ===============================================
//////////// FONCTION : MASQUER CERTAINES RÉVISIONS DES MODIFICATIONS RECENTES //////////////

// Change le style du premier élément parent "table" de chaque élément "a" dont l'attribut "title" commence par la valeur indiquée.
$(document).ready(function(){
      if (wgPageName == "Spécial:Modifications_récentes") {
            $( "a[title^='Utilisateur\\3A Bachirx\\/']" ).closest("table").css({"display":"none"});
      }
});
// ===============================================
