<?php
/**
 * Wikimini - Modern version of MonoBook with fresh look and many usability
 * improvements.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Skins
 */

/**
 * SkinTemplate class for Wikimini skin
 * @ingroup Skins
 */
class SkinWikimini extends SkinTemplate {
	public $skinname = 'wikimini';
	public $stylename = 'Wikimini';
	public $template = 'WikiminiTemplate';
	/**
	 * @var Config
	 */
	private $wikiminiConfig;

	public function __construct() {
		$this->wikiminiConfig = ConfigFactory::getDefaultInstance()->makeConfig( 'wikimini' );
	}

	/**
	 * Initializes output page and sets up skin-specific parameters
	 * @param OutputPage $out Object to initialize
	 */
	public function initPage( OutputPage $out ) {
		parent::initPage( $out );

		$out->addMeta( 'viewport', 'width=device-width, initial-scale=1.0' );

		// Append CSS which includes IE only behavior fixes for hover support -
		// this is better than including this in a CSS file since it doesn't
		// wait for the CSS file to load before fetching the HTC file.
		$min = $this->getRequest()->getFuzzyBool( 'debug' ) ? '' : '.min';
		$out->addHeadItem( 'csshover',
			'<!--[if lt IE 7]><style type="text/css">body{behavior:url("' .
				htmlspecialchars( $this->getConfig()->get( 'LocalStylePath' ) ) .
				"/{$this->stylename}/resources/htc/csshover{$min}.htc\")}</style><![endif]-->"
		);

		if ( WIKIMINI_PROJECT_UID === 'sv' ) {
			$out->addModules( array( 'swiffy.sv.js' ) );
		} else {
			$out->addModules( array( 'swiffy.fr.js' ) ) ;
		}

		$out->addModules( array( 'skins.wikimini.js' ) );
		$out->addModules( array( 'skins.wikimini.js2' ) );

	}

	/**
	 * Loads skin and user CSS files.
	 * @param OutputPage $out
	 */
	function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );

		/* 20161230/Nixit
		$styles = array( 'mediawiki.skinning.interface', 'skins.wikimini.styles' );
		wfRunHooks( 'SkinWikiminiStyleModules', array( $this, &$styles ) );
		$out->addModuleStyles( $styles );
		*/

		$styles = [ 'mediawiki.skinning.interface', 'skins.wikimini.styles' ];
		Hooks::run( 'SkinWikiminiStyleModules', [ $this, &$styles ] );
		$out->addModuleStyles( $styles );

	}

	/**
	 * Override to pass our Config instance to it
	 */
	public function setupTemplate( $classname, $repository = false, $cache_dir = false ) {
		return new $classname( $this->wikiminiConfig );
	}

	public function addSkinModulesToOutput() {
		// load Bootstrap scripts
		$out = $this->output;
		$out->addModules( $this->getComponentFactory()->getRootComponent()->getResourceLoaderModules() );
	}

}
