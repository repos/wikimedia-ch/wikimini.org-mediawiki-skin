<?php
/**
 * Wikimini based on Wikimini - Modern version of MonoBook with fresh look and many usability
 * improvements.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Skins
 */

/**
 * QuickTemplate class for Wikimini skin
 * @ingroup Skins
 */
class WikiminiTemplate extends BaseTemplate {
	private $wikiPath = "";
	private $skinPath = "";
	private $imagesPath ="";
	private $forumTag = "AWCforum";
    /* Functions */

    /**
     * Outputs the entire contents of the (X)HTML page
     */
    public function execute() {
    	global $wgStylePath;
    	global $wgScriptPath;
	global $wgGoogleAnalytics;
    	$this->wikiPath =  $wgScriptPath;
    	$this->skinPath = $wgStylePath .'/Wikimini/';
    	$this->imagesPath = $this->skinPath.'resources/images/';
        // Build additional attributes for navigation urls
        $nav = $this->data['content_navigation'];

        if ( $this->config->get( 'WikiminiUseIconWatch' ) ) {
            $mode = $this->getSkin()->getUser()->isWatched( $this->getSkin()->getRelevantTitle() )
                ? 'unwatch'
                : 'watch';

            if ( isset( $nav['actions'][$mode] ) ) {
                $nav['views'][$mode] = $nav['actions'][$mode];
                $nav['views'][$mode]['class'] = rtrim( 'icon ' . $nav['views'][$mode]['class'], ' ' );
                $nav['views'][$mode]['primary'] = true;
                unset( $nav['actions'][$mode] );
            }
        }
        $xmlID = '';
        foreach ( $nav as $section => $links ) {
            foreach ( $links as $key => $link ) {
                if ( $section == 'views' && !( isset( $link['primary'] ) && $link['primary'] ) ) {
                    $link['class'] = rtrim( 'collapsible ' . $link['class'], ' ' );
                }

                $xmlID = isset( $link['id'] ) ? $link['id'] : 'ca-' . $xmlID;
                $nav[$section][$key]['attributes'] =
                    ' id="' . Sanitizer::escapeId( $xmlID ) . '"';
                if ( $link['class'] ) {
                    $nav[$section][$key]['attributes'] .=
                        ' class="' . htmlspecialchars( $link['class'] ) . '"';
                    unset( $nav[$section][$key]['class'] );
                }
                if ( isset( $link['tooltiponly'] ) && $link['tooltiponly'] ) {
                    $nav[$section][$key]['key'] =
                        Linker::tooltip( $xmlID );
                } else {
                    $nav[$section][$key]['key'] =
                        Xml::expandAttributes( Linker::tooltipAndAccesskeyAttribs( $xmlID ) );
                }
            }
        }
        $this->data['namespace_urls'] = $nav['namespaces'];
        $this->data['view_urls'] = $nav['views'];
        $this->data['action_urls'] = $nav['actions'];
        $this->data['variant_urls'] = $nav['variants'];

        // Reverse horizontally rendered navigation elements
        if ( $this->data['rtl'] ) {
            $this->data['view_urls'] =
                array_reverse( $this->data['view_urls'] );
            $this->data['namespace_urls'] =
                array_reverse( $this->data['namespace_urls'] );
            $this->data['personal_urls'] =
                array_reverse( $this->data['personal_urls'] );
        }
        //Flash button & hide menu condition

        //20161227/Nixit
        $flashbut = "1"; //default
        $hidemenu1 = "0"; //default
        //

	$my_title = $_GET['title'] ?? '';

        if ($my_title == $this->getMsg('mainpage')) { $hidemenu1 ="1";} //it's the main page
        else if ($my_title == $this->getMsg('wikimini-link_children_pagename')) { $flashbut = "2"; $hidemenu1 ="1";} // it's the childs portal
        else if ($my_title == $this->getMsg('wikimini-link_adults_pagename')) { $flashbut = "3"; $hidemenu1 ="1";} // it's the adults portal
        else if ($my_title == $this->getMsg('wikimini-link_teachers_pagename')) { $flashbut = "4"; $hidemenu1 ="1";} // it's the teachers portal
        else if (substr($my_title, 9, 8) == "AWCforum") { $flashbut = "5"; $hidemenu2 ="1";} // it's the forum
        else if (substr($my_title, 0, 11) == "Discussion:") { $flashbut = "6";} // it's a talk page
        else if (substr($my_title, 0, 11) == "Discussion_") { $flashbut = "6";} // it's a user talk page

        /* 20161227/Nixit
        else
        {
        	$flashbut = "1";
        }
        */

        // Output HTML Page

        ?>
        <?php
        $this->html( 'headelement' );
        ?>
	<script>
		var flashButton ='<?php echo "button=$flashbut"?>'
	</script>
    <!-- modal -->
    <div class="modal" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title">Upload</h4>

                </div>
                <div class="modal-body">
                    <iframe id="stockIframe" data-src="<?php echo htmlspecialchars($this->data['nav_urls']['upload']['href']) ?>" src="" width="770" height="500"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
	<div class="mw-body container" role="main">
	<?php if (!$this->data['printable']) { ?>
	<?php echo $this->renderMobileSearchBox(); ?>
		<div class="row hidden-xs">
			<div id="swiffycontainer" style="width: 760px; height: 278px">
            		</div>
		</div>
        <div class="row visible-xs-block" id="mobileNav">
			<nav class="navbar navbar-default navbar-wikimini">
			  <div class="container-fluid">
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNav" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>

				<a href="<?php echo htmlspecialchars( $this->data['nav_urls']['mainpage']['href'] ) ?>" class="navbar-brand">
					<img src="<?php $this->text( 'logopath' ) ?>"	alt="<?php $this->text( 'sitename' ) ?>"/>
				</a>

			    </div>

			    <!-- Nav -->
			    <div class="collapse navbar-collapse" id="mainNav">
			      <ul class="nav navbar-nav">
			        <li class="active wm-home"><a href="/" title="<?php echo $this->msg('wikimini-link_home_title'); ?>"><?php echo $this->msg('wikimini-link_home_text'); ?></a></li>
			        <li class="wm-children"><a href="/wiki/<?php echo $this->msg('wikimini-link_children_pagename'); ?>" title="<?php echo $this->msg('wikimini-link_children_title'); ?>"><?php echo $this->msg('wikimini-link_children_text'); ?></a></li>
			        <li class="wm-adults"><a href="/wiki/<?php echo $this->msg('wikimini-link_adults_pagename'); ?>" title="<?php echo $this->msg('wikimini-link_adults_title'); ?>"><?php echo $this->msg('wikimini-link_adults_text'); ?></a></li>
			        <li class="wm-teachers"><a href="/wiki/<?php echo $this->msg('wikimini-link_teachers_pagename'); ?>" title="<?php echo $this->msg('wikimini-link_teachers_title'); ?>"><?php echo $this->msg('wikimini-link_teachers_text'); ?></a></li>
			        <li class="wm-forum"><a href="/wiki/Special:AWCforum" title="<?php echo $this->msg('wikimini-link_forum_title'); ?>" title="<?php echo $this->msg('wikimini-link_forum_text'); ?>"><?php echo $this->msg('wikimini-link_forum_text'); ?></a></li>
			        <li class="wm-help"><a href="/wiki/<?php echo $this->msg('wikimini-link_help_pagename'); ?>" title="<?php echo $this->msg('wikimini-link_help_title'); ?>"><?php echo $this->msg('wikimini-link_help_text'); ?></a></li>
			      </ul>
			    </div>
			  </div>
			</nav>
        </div>
        <div class="row">

	<?php
	if (
		$this->data['sitenotice'] &&
		strcmp($my_title, substr($this->data['nav_urls']['mainpage']['href'],strrpos($this->data['nav_urls']['mainpage']['href'], "/")+1) ) != 0
	) {
                ?>
                <div id="siteNotice"><?php $this->html( 'sitenotice' ) ?></div>
            <?php
            }
            ?>
            <?php
            if ( is_callable( array( $this, 'getIndicators' ) ) ) {
                echo $this->getIndicators();
            }
            ?>

      </div>
      <div class="row">
      <?php
			if($hidemenu1 <>'1') {
				$this->renderMobileOptionbox();
			} ?>
      </div>

	<?php } ?>
        <div id="fluid-row" class="row  fluid-candidate">

			<?php if (!$this->data['printable']  &&  substr_count(strtolower($my_title), strtolower($this->forumTag))<1) { ?>
	        <div id="fluid-content" class="col-md-7">
			<?php }else{ ?>
	        <div class="col-md-12 col-lg-12">

			<?php } ?>
	        <div  id="content">
			<h1 id="firstHeading" class="firstHeading" lang="<?php
			$this->data['pageLanguage'] = $this->getSkin()->getTitle()->getPageViewLanguage()->getHtmlCode();
			$this->text( 'pageLanguage' );
			?>">
			<?php
			if ($my_title == $this->getMsg('mainpage')) {
				 echo $this->msg('wikimini-home_title');
			}else{

				if ($this->data['displaytitle']!=""){
				echo "EE";
					$this->html('title');
				}else{
					$this->text('title') ;
					}
			}
			?>
			</h1>

			</h1>
			<?php $this->html( 'prebodyhtml' ) ?>
			<div id="bodyContent" class="mw-body-content">
				<?php
				if ( $this->data['isarticle'] && $hidemenu1 <>'1') {
					?>
					<div id="siteSub"><?php $this->msg( 'tagline' ) ?></div>
				<?php
				}
				?>
				<div id="contentSub"<?php
				$this->html( 'userlangattributes' )
				?>><?php $this->html( 'subtitle' ) ?></div>
				<?php
				if ( $this->data['undelete'] ) {
					?>
					<div id="contentSub2"><?php $this->html( 'undelete' ) ?></div>
				<?php
				}
				?>
				<?php
				if ( $this->data['newtalk'] ) {
					?>
					<div class="usermessage"><?php $this->html( 'newtalk' ) ?></div>
				<?php
				}
				?>
				<div id="jump-to-nav" class="mw-jump">
					<?php $this->msg( 'jumpto' ) ?>
					<a href="#mw-head"><?php
						$this->msg( 'jumptonavigation' )
						?></a><?php
					$this->msg( 'comma-separator' )
					?>
					<a href="#p-search"><?php $this->msg( 'jumptosearch' ) ?></a>
				</div>
				<?php $this->html( 'bodycontent' ) ?>
				<?php if($this->data['catlinks']) { ?><div id="catlinks"><?php $this->html('catlinks') ?></div><?php } ?><!-- end content -->


				<?php
				if ( $this->data['dataAfterContent'] ) {
					?>
					<?php
					$this->html( 'dataAfterContent' );
					?>
				<?php
				}
				?>
	        	</div>
	        </div>



	        </div>

			<?php if ( !$this->data['printable']  &&  substr_count(strtolower($my_title ?? ''), strtolower($this->forumTag))<1) { ?>
	        <div  id="fluid-sidebar" class="col-md-5  text-center"  id="bodyTools">


			<!-- Options (print, home, etc.)  -->
				<div class="OptionsBox">
				   <div class="visualClear"><span id="OptionsTexte" class="OptionsTexte_Hidden"></span></div>
				   <div id="MenuOptions">
				  <!--   <?php //$this->msg( 'tagline' ) ?>-->
             <a id="HeaderButton" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-reduce-header' ) ?>" data-text1="<?php $this->msg( 'wikimini-tooltip-reduce-header' ) ?>"  data-text2="<?php $this->msg( 'wikimini-tooltip-show-header' ) ?>">
              <img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>hide-header-b.png" alt="$this->msg( 'wikimini-tooltip-reduce-header' ) ?>"></a>

					   <img src="<?php echo $this->imagesPath; ?>blank-18x18.png" width="9">

					   <a id="SiteWidth" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-wide-site' ) ?>" data-text1="<?php $this->msg( 'wikimini-tooltip-wide-site' ) ?>"  data-text2="<?php $this->msg( 'wikimini-tooltip-narrow-site' ) ?>">
             <img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>expand-b.png" alt="<?php $this->msg( 'wikimini-tooltip-wide-site' ) ?>"></a>


					   <img src="<?php echo $this->imagesPath; ?>blank-18x18.png" width="9">

					   <a id="IncreaseFont" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-increase-font' ) ?>">
             <img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>font-plus-b.png" alt="<?php $this->msg( 'wikimini-tooltip-increase-font' ) ?>"></a>
					   <a id="DecreaseFont" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-reduce-font' ) ?>">
              <img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>font-minus-b.png" alt="<?php $this->msg( 'wikimini-tooltip-reduce-font' ) ?>"></a>
					   <a id="DefaultFontSize" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-reset-font' ) ?>">
					   	<img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>font-default-b.png" alt="<?php $this->msg( 'wikimini-tooltip-reset-font' ) ?>"></a>

					   <img src="<?php echo $this->imagesPath; ?>blank-18x18.png" width="9">

					   <a id="PrintVersion" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-print-version' ) ?>">
					   	<img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>printer-b.png" alt="<?php $this->msg( 'wikimini-tooltip-print-version' ) ?>"></a>
				   </div>
				</div>

			<?php
			if($hidemenu1 <>'1'){
				$this->renderOptionBox();
			}

			?>
			<?php if($this->data['notspecialpage']) { //Je cache tous les menus sur les pages sp�ciales car certaines n�cessitent plus d'espace (les galeries par exemple) ?>
			<!-- search -->
			<?php $this->renderSearchBox() ; ?>

	        <!-- Daily Picture -->
	        <?php
	        //display only if the main page is displayed
	        if (strcmp($my_title, substr($this->data['nav_urls']['mainpage']['href'],strrpos($this->data['nav_urls']['mainpage']['href'], "/")+1) ) == 0 )
	        	$this->renderDailyPicture();
	        ?>

	        <!-- Toolbox -->
			<?php
	        $this->renderToolbox();
	        ?>
	        <!--  alert - abuse -->
	        	<p style="text-align:center;">
	        		<a href="/wiki/<?php $this->msg('wikimini-link_abuse_pagename'); ?>" title="<?php echo $this->msg('wikimini-link_abuse_title'); ?>" target="_blank"><img src="<?php echo $this->imagesPath; ?>wikiboo13_36x36_trans.png" alt="Wikiboo" /><br /><?php echo  $this->msg('wikimini-link_abuse_text'); ?></a>
	        		</p>
		<?php } //fin du if notspecialpage ?>
	        </div>
            <?php } ?>

        </div>
          <?php if ( !$this->data['printable'] ) { ?>

        <div class="row   fluid-candidate">

	        	<div class="content-bottom">
					<h5><img src="<?php echo $this->imagesPath; ?>wikiboo12_36x36_trans.png" alt="Wikiboo" />&nbsp;<?php $this->msg('personaltools') ?></h5>
					<div class="box-bottom">

						<div class="" style="padding: 15px;">
							<ul>
					<?php foreach($this->data['personal_urls'] as $key => $item) { ?>
								<li id="pt-<?php echo Sanitizer::escapeId($key) ?>"<?php
									if ($item['active']) { ?> class="active"<?php } ?>><a href="<?php
								echo htmlspecialchars($item['href']) ?>"<?php echo $this->tooltipAndAccesskey('pt-'.$key) ?><?php
								if(!empty($item['class'])) { ?> class="<?php
								echo htmlspecialchars($item['class']) ?>"<?php } ?>><?php
								echo htmlspecialchars($item['text']) ?></a></li>
					<?php } ?>
							</ul>
						</div>

					</div>
				</div>
			</div>
			<?php } ?>

        <div class="row   fluid-candidate">
				<!--footer -->
				<div class="col-md-12 text-center" id="footer">
					<ul id="f-list" style="padding-bottom:8px;">
				<?php
						$footerlinks = array(
							'viewcount', 'lastmod', 'numberofwatchingusers', 'credits', 'copyright',
							'about', 'disclaimer', 'privacy', 'tagline',
						);
						foreach( $footerlinks as $aLink ) {
							if( isset( $this->data[$aLink] ) && $this->data[$aLink] ) {
				?>				<li id="<?php echo $aLink?>"><?php $this->html($aLink) ?></li>
				<?php 		}
						}
				?>
		                   		<?php echo $this->msgHtml('wikimini-additional_footer_links'); ?>

							</ul>
							<p class="center"><?php echo $this->msgHtml('wikimini-footer_title'); ?>
							&nbsp;|&nbsp;
							<a rel="nofollow" href="http://validator.w3.org/check/referer"><img src="<?php echo $this->imagesPath; ?>validator-xhtml.gif" alt="Wiki valide XHTML 1.0" /></a>&nbsp;
							<a rel="nofollow" href="http://jigsaw.w3.org/css-validator/check/referer?profile=css3"><img src="<?php echo $this->imagesPath; ?>validator-css.gif" alt="Wiki valide CSS 3" /></a>&nbsp;
							<a rel="nofollow" href="http://www.w3.org/WAI/WCAG1A-Conformance"><img src="<?php echo $this->imagesPath; ?>validator-aaa.gif" alt="Wiki valide WCAG1A" /></a>&nbsp;
							<a href="/wiki/<?php echo $this->msgHtml('wikimini-link_syndication_pagename'); ?>"><img src="<?php echo $this->imagesPath; ?>rss-xml.gif" alt="<?php echo $this->msg('wikimini-link_syndication_title'); ?>" /></a>

							</p>

						</div>
						<!--footer end-->



        </div>
	</div>

<?php $this->html('reporttime') ?>
<?php if ( $this->data['debug'] ): ?>
<!-- Debug output:
<?php $this->text( 'debug' ); ?>

-->
<?php endif;

	$this->printTrail();

/************ GOOGLE ANALYTICS ***********/
 if (strlen($wgGoogleAnalytics)){

 ?>
<!--
 <script type="text/javascript">
 		  var _gaq = _gaq || [];
 		  _gaq.push(['_setAccount', '<?php echo $wgGoogleAnalytics; ?>']);
 		  _gaq.push(['_trackPageview']);

 		  (function() {
 		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
 		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
 		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  		 })();
 </script>
-->
 <?php
 }
/************ GOOGLE ANALYTICS ***********/
?>
    </body>
</html>
<?php
    }


    protected function renderOptionBox(){
 ?>
								<div class="option-box text-center">
									<div class="text-left">

													<h5><img src="<?php  echo $this->imagesPath; ?>wikiboo06_36x36_trans.png"  alt="Wikiboo" /> <?php $this->msg('views') ?></h5>
													<br />
													<div class="content">
														<ul>
												<?php

												$list = array( 'nstab-main'=>1, 'nstab-user'=>1, 'talk'=>1, 've-edit'=>1, 'edit'=>1, 'move'=>1, 'history'=>1, 'watch'=>1, 'unwatch'=>1, );

												/** edit/ve-edit actions only for articles if user not logged in **/
												if (!$this->getSkin()->getUser()->isLoggedIn() && !$this->data['isarticle']){
													unset($list['ve-edit']);
													unset($list['edit']);
												}else{
													//edit link for anonymous users
													//only if the page is not in edit mode and if it is an article that exists
													
													/* 20161230/Nixit
													if (!$_GET['action']=='edit' && !$_GET['veaction']=='edit' && $this->data['isarticle'] &&$this->data['articleid'] > 0){
													*/
	
													if ((array_key_exists('action', $_GET) && !$_GET['action']=='edit') &&
														(array_key_exists('veaction', $_GET) && !$_GET['veaction']=='edit') &&
														(array_key_exists('isarticle', $_GET) && $this->data['isarticle']) && 
														$this->data['articleid'] > 0){
													//end
														$anonEdit['class']='';
														$anonEdit['text']=$this->getMsg('edit');
														$anonEdit['href']=$this->wikiPath.'?title='.$this->getSkin()->getTitle().'&action=edit';
														$anonEdit['primary']=1;
														$anonEdit['id']='ca-edit';
														$this->data['content_actions']['edit']=$anonEdit;
													}

												}

												if (substr_count(strtolower($this->data['title']), strtolower($this->getMsg("wikimini-category"))) > 0)
													unset($list['ve-edit']);

												/** patch for sysop **/
												if (in_array ('sysop', $this->getSkin()->getUser()-> getAllGroups())){
													$list['protect']=1;
													$list['delete']=1;
												}
												foreach($this->data['content_actions'] as $key => $tab) {

													if (!isset($list[$key]))
														continue;
													if (strcmp($key,'move')==0 && isset($this->data['namespace_urls']['user']))
														continue;

													//echo  ;

													?>
																<li class="ca-<?php echo Sanitizer::escapeId($key) ?>"<?php
					 												if($tab['class']) { ?> class="<?php echo htmlspecialchars($tab['class']) ?>"<?php }
																 ?>><a href="<?php echo htmlspecialchars($tab['href']) ?>"<?php echo $this->tooltipAndAccesskey('ca-'.$key) ?>><?php
																 echo htmlspecialchars($tab['text']) ?></a></li>
												<?php
													if (strcmp($key, 'edit')==0){
															?>
																<li class="ca-<?php echo Sanitizer::escapeId($key) ?>" id="classify"<?php
					 												if($tab['class']) { ?> class="<?php echo htmlspecialchars($tab['class']) ?>"<?php }
																 ?>><a href="<?php echo htmlspecialchars($tab['href']) ?>&classify=1"<?php echo $this->tooltipAndAccesskey('ca-'.$key) ?>><?php
																 echo $this->getMsg("wikimini-link_classify_page"); //htmlspecialchars($tab['text'])
																 ?></a></li>
												<?php
													}
												}


													//print_r($this->data['content_actions']);
													/*
													 [isarticle] => 1								 *
													[edit] => Array
													(
															[class] =>
															[text] => Edit
															[href] => /index.php?title=Accueil&action=edit
															[primary] => 1
															[id] => ca-edit
													)
													*/

// 													/** edit link for anonym users **/
// 													if (!$this->getSkin()->getUser()->isLoggedIn() && $this->data['isarticle']){

												?>

														</ul>
														<p class="p1">
														<img src="<?php  echo $this->imagesPath; ?>home.png" alt="<?php $this->msg('tooltip-p-logo') ?>" />&nbsp;&nbsp;
														<a class="blackbold" href="<?php echo $this->wikiPath; ?>" title="<?php $this->msg('tooltip-n-mainpage') ?> [z]" accesskey="z"><?php $this->msg('tooltip-p-logo') ?></a>
														</p>
													</div>
									</div>
								</div>
		<?php

	}

	protected function getRandomPictureKids(){
		global $wgStylePath;
		// Random Image
		$total = "116";

		// Change to the type of files to use eg. .jpg or .gif
		$file_type = ".jpg";
		$file_prefix = "wikimini-kids-";

		// Change to the location of the folder containing the images
		$image_folder = $this->imagesPath."kids";

		// You do not need to edit below this line
		$start = "1";
		$random = mt_rand($start, $total);
		$image_name = "$file_prefix$random$file_type";

		return "<img src=\"$image_folder/$image_name\" alt=\"Wikimini, l'encyclopédie pour enfants et adolescents\" />";

	}


	protected function renderMobileOptionbox() {
		global $wgStylePath;
		?>
			<div class="mobile-tool-box text-center">
			<div>
	    		<div class="dropdown">
					  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					    <?php $this->msg('views')  ?>
					    <span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

								<?php
												$list = array( 'talk'=>1, 've-edit'=>1, 'edit'=>1, 'move'=>1, 'history'=>1, 'watch'=>1, 'unwatch'=>1, );

												/** edit/ve-edit actions only for articles if user not logged in **/
												if (!$this->getSkin()->getUser()->isLoggedIn() && !$this->data['isarticle']){
													unset($list['ve-edit']);
													unset($list['edit']);
												}

												/** patch for sysop **/
												if (in_array ('sysop', $this->getSkin()->getUser()-> getAllGroups())){
													$list['protect']=1;
													$list['delete']=1;
												}
												foreach($this->data['content_actions'] as $key => $tab) {
													if (!isset($list[$key]))
														continue;
													if (strcmp($key,'move')==0 && isset($this->data['namespace_urls']['user']))
														continue;

													//echo  ;

													?>
																<li class="ca-<?php echo Sanitizer::escapeId($key) ?>"<?php
					 												if($tab['class']) { ?> class="<?php echo htmlspecialchars($tab['class']) ?>"<?php }
																 ?>><a href="<?php echo htmlspecialchars($tab['href']) ?>"<?php echo $this->tooltipAndAccesskey('ca-'.$key) ?>><?php
																 echo htmlspecialchars($tab['text']) ?></a></li>
												<?php	} ?>


														<li class="ca-home"><a href="/" title="<?php $this->msg('tooltip-n-mainpage') ?> [z]" accesskey="z"><?php $this->msg('tooltip-p-logo') ?></a></li>


					  </ul>
				</div>
			</div>
	    </div>
	    		<?php

	    }



    protected function renderToolbox() {
    	global $wgStylePath;
    	?>
		<div class="tool-box text-center">
			<div class="text-left">
	    		<h5>
	    			<img src="<?php  echo $this->imagesPath; ?>wikiboo11_36x36_trans.png"  alt="Wikiboo" />
	    		    		<?php $this->msg('toolbox') ?>
	    		</h5>
    		</div>
    			<?php
					echo $this->getRandomPictureKids();
				?>

			<div class="tool-box-content text-left">

				<div class="text-left">
				    <ul>
				    	<li id="t-newarticle"><a href="/wiki/<?php echo $this->msg('wikimini-link_new_article_pagename'); ?>" title="<?php echo $this->msg('wikimini-link_new_article'); ?>"><?php echo $this->msg('wikimini-link_new_article'); ?></a></li>

			    <?php
			    	if($this->data['nav_urls']['upload']) { ?>
			    		<li id="t-upload">
                            <a id="uploadBtn"><?php $this->msg('upload')?></a>
                        </li>
			    	<?php
			    	}
			    	?>
				<?php foreach( array('contributions', 'log', 'blockip', 'emailuser', 'specialpages') as $special ){ if($this->data['nav_urls'][$special]) { ?>
				    	<li id="t-<?php echo $special ?>"><a href="<?php echo htmlspecialchars($this->data['nav_urls'][$special]['href']) ?>"<?php echo $this->tooltipAndAccesskey('t-'.$special) ?>><?php $this->msg($special) ?></a></li>
				    <?php 	} }
				if($this->data['notspecialpage']) { ?>			<li id="t-whatlinkshere"><a href="<?php
				    		echo htmlspecialchars($this->data['nav_urls']['whatlinkshere']['href'])
				    		?>"<?php echo $this->tooltipAndAccesskey('t-whatlinkshere') ?>><?php $this->msg('whatlinkshere') ?></a></li>
																	<?php 		}
				    if(isset($this->data['nav_urls']['trackbacklink'])) { ?>
				    	<li id="t-trackbacklink"><a href="<?php
				    		echo htmlspecialchars($this->data['nav_urls']['trackbacklink']['href'])
				    		?>"<?php echo $this->tooltipAndAccesskey('t-trackbacklink') ?>><?php $this->msg('trackbacklink') ?></a></li>
																	<?php 	}
				    if($this->data['feeds']) { ?>
				    	<li id="feedlinks"><?php foreach($this->data['feeds'] as $key => $feed) {
				    			?><span id="feed-<?php echo Sanitizer::escapeId($key) ?>"><a href="<?php
				    			echo htmlspecialchars($feed['href']) ?>"<?php echo $this->tooltipAndAccesskey('feed-'.$key) ?>><?php echo htmlspecialchars($feed['text'])?></a>&nbsp;</span>
				    			<?php } ?></li><?php
				    }
			    ?>
    			</ul>
				    <p class="p1">
				    <img src="<?php echo $this->imagesPath; ?>award_star_gold_1.png" alt="<?php echo $this->msg('wikimini-link_wikichampions'); ?>" />&nbsp;&nbsp;<a class="blackbold" href="/wiki/Special:ContributionScores" title="<?php echo $this->msg('wikimini-link_wikichampions'); ?>"><?php echo $this->msg('wikimini-link_wikichampions'); ?></a><br />
				    <img src="<?php echo $this->imagesPath; ?>bell.png" alt="<?php $this->msg('wantedpages') ?>" />&nbsp;&nbsp;<a class="blackbold" href="/wiki/Special:WantedPages" title="<?php $this->msg('wantedpages') ?>"><?php $this->msg('wantedpages') ?></a><br />
				    <img src="<?php echo $this->imagesPath; ?>clock.png" alt="<?php $this->msg('recentchanges') ?>" />&nbsp;&nbsp;<a class="blackbold" href="/wiki/Special:RecentChanges" title="<?php $this->msg('tooltip-n-recentchanges') ?>"><?php $this->msg('recentchanges') ?></a><br />
				    </p>
														</div>





			</div>
		</div>

    		<?php

    }


	protected function renderDailyPicture() {
		global $wgStylePath;
		?>
    	<div class="dailyPic">

			<img src="<?php  echo $this->imagesPath; ?>Wikimini-daily_picture-<?php $this->text('lang') ?>.png" alt="<?php echo $this->msg('wikimini-picture_of_the_day'); ?>" style="-moz-transform:rotate(2deg); -webkit-transform:rotate(2deg);" />
			<div id="multicouche">
				<?php if (isset($_SERVER['HTTP_HOST'])) {
					if ( WIKIMINI_PROJECT_UID === 'sv' ) {
						include_once ('pictures-of-the-day/sv/picture-of-the-day.html');
					} elseif ( WIKIMINI_PROJECT_UID === 'fr' ) {
						include_once ('pictures-of-the-day/fr/picture-of-the-day.html');
					}
				} ?>
			</div>
		</div>

		<?php

	}


    protected function renderSearchBox() {
		global $wgStylePath;

    	?>
	        	<div id="p-search" role="search" class="search-box text-left">
	        		<div class="search-logo">
	        		<img src="<?php  echo $this->imagesPath; ?>wikiboo08_36x36_trans.png" alt="Wikiboo">
	        		</div>
	        		<div class="search-content">
						<h5<?php $this->html( 'userlangattributes' ) ?>>
							<?php $this->msg( 'search' ) ?>
						</h5>
						<form action="<?php $this->text( 'wgScript' ) ?>" id="searchform">
							<?php
							if ( $this->config->get( 'WikiminiUseSimpleSearch' ) ) {
							?>
							<div id="simpleSearch">
								<?php
							} else {
							?>
								<div>
									<?php
							}
							?>
							<?php
							echo $this->makeSearchInput( array( 'id' => 'searchInput' ) );
							echo Html::hidden( 'title', $this->get( 'searchtitle' ) );
							echo $this->makeSearchButton(
								'fulltext',
								array( 'id' => 'mw-searchButton', 'class' => 'searchButton mw-fallbackSearchButton' )
							);
							echo $this->makeSearchButton(
								'go',
								array( 'id' => 'searchButton', 'class' => 'searchButton' )
							);
							?>
								</div>
						</form>
					</div>
				</div>

    	<?php
    	}


    	protected function renderMobileSearchBox() {
    		global $wgStylePath;

    		?>
    		        	<div id="mobile-p-search" role="search" class="mobile-search-box text-left">
    		        		<div class="search-content">

    							<form action="<?php $this->text( 'wgScript' ) ?>" id="searchform">
    								<?php
    								if ( $this->config->get( 'WikiminiUseSimpleSearch' ) ) {
    								?>
    								<div id="simpleSearch">
    									<?php
    								} else {
    								?>
    									<div>
    										<?php
    								}
    								?>
    								<?php
    								echo $this->makeSearchInput( array( 'id' => 'searchInput' ) );
    								echo Html::hidden( 'title', $this->get( 'searchtitle' ) );
    								echo $this->makeSearchButton(
    									'fulltext',
    									array( 'id' => 'mw-searchButton', 'class' => 'searchButton mw-fallbackSearchButton' )
    								);
    								echo $this->makeSearchButton(
    									'go',
    									array( 'id' => 'searchButton', 'class' => 'searchButton' )
    								);
    								?>
    									</div>
    							</form>
    						</div>
    					</div>

    	    	<?php
    	    	}



    /**
     * Render a series of portals
     *
     * @param array $portals
     */
    protected function renderPortals( $portals ) {
        // Force the rendering of the following portals
        if ( !isset( $portals['SEARCH'] ) ) {
            $portals['SEARCH'] = true;
        }
        if ( !isset( $portals['TOOLBOX'] ) ) {
            $portals['TOOLBOX'] = true;
        }
        if ( !isset( $portals['LANGUAGES'] ) ) {
            $portals['LANGUAGES'] = true;
        }
        // Render portals
        foreach ( $portals as $name => $content ) {
            if ( $content === false ) {
                continue;
            }

            // Numeric strings gets an integer when set as key, cast back - T73639
            $name = (string)$name;

            switch ( $name ) {
                case 'SEARCH':
                    break;
                case 'TOOLBOX':
                    $this->renderPortal( 'tb', $this->getToolbox(), 'toolbox', 'SkinTemplateToolboxEnd' );
                    break;
                case 'LANGUAGES':
                    if ( $this->data['language_urls'] !== false ) {
                        $this->renderPortal( 'lang', $this->data['language_urls'], 'otherlanguages' );
                    }
                    break;
                default:
                    $this->renderPortal( $name, $content );
                    break;
            }
        }
    }

    /**
     * @param string $name
     * @param array $content
     * @param null|string $msg
     * @param null|string|array $hook
     */
    protected function renderPortal( $name, $content, $msg = null, $hook = null ) {
        if ( $msg === null ) {
            $msg = $name;
        }
        $msgObj = wfMessage( $msg );
        ?>

        <div class="border-green">
        <div class="portal" role="navigation" id='<?php
        echo Sanitizer::escapeId( "p-$name" )
        ?>'<?php
        echo Linker::tooltip( 'p-' . $name )
        ?> aria-labelledby='<?php echo Sanitizer::escapeId( "p-$name-label" ) ?>'>
            <h3<?php
            $this->html( 'userlangattributes' )
            ?> id='<?php
            echo Sanitizer::escapeId( "p-$name-label" )
            ?>'><?php
                echo htmlspecialchars( $msgObj->exists() ? $msgObj->text() : $msg );
                ?></h3>

            <div class="body">
                <?php
                if ( is_array( $content ) ) {
                    ?>
                    <ul>
                        <?php
                        foreach ( $content as $key => $val ) {
                            ?>
                            <?php echo $this->makeListItem( $key, $val ); ?>

                        <?php
                        }
                        if ( $hook !== null ) {
                            wfRunHooks( $hook, array( &$this, true ) );
                        }
                        ?>
                    </ul>
                <?php
                } else {
                    ?>
                    <?php
                    echo $content; /* Allow raw HTML block to be defined by extensions */
                }

                $this->renderAfterPortlet( $name );
                ?>
            </div>
        </div>
        </div>

    <?php
    }

    /**
     * Render one or more navigations elements by name, automatically reveresed
     * when UI is in RTL mode
     *
     * @param array $elements
     */
    protected function renderNavigation( $elements ) {
    ?><div class="border-red"><?php
        // If only one element was given, wrap it in an array, allowing more
        // flexible arguments
        if ( !is_array( $elements ) ) {
            $elements = array( $elements );
            // If there's a series of elements, reverse them when in RTL mode
        } elseif ( $this->data['rtl'] ) {
            $elements = array_reverse( $elements );
        }
        // Render elements
        foreach ( $elements as $name => $element ) {
            switch ( $element ) {
                case 'NAMESPACES':
                    ?>
                    <div id="p-namespaces" role="navigation" class="wikiminiTabs<?php
                    if ( count( $this->data['namespace_urls'] ) == 0 ) {
                        echo ' emptyPortlet';
                    }
                    ?>" aria-labelledby="p-namespaces-label">
                        <h3 id="p-namespaces-label"><?php $this->msg( 'namespaces' ) ?></h3>
                        <ul<?php $this->html( 'userlangattributes' ) ?>>
                            <?php
                            foreach ( $this->data['namespace_urls'] as $link ) {
                                ?>
                                <li <?php
                                echo $link['attributes']
                                ?>><span><a href="<?php
                                        echo htmlspecialchars( $link['href'] )
                                        ?>" <?php
                                        echo $link['key']
                                        ?>><?php
                                            echo htmlspecialchars( $link['text'] )
                                            ?></a></span></li>
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                    break;
                case 'VARIANTS':
                    ?>
                    <div id="p-variants" role="navigation" class="wikiminiMenu<?php
                    if ( count( $this->data['variant_urls'] ) == 0 ) {
                        echo ' emptyPortlet';
                    }
                    ?>" aria-labelledby="p-variants-label">
                        <?php
                        // Replace the label with the name of currently chosen variant, if any
                        $variantLabel = $this->getMsg( 'variants' )->text();
                        foreach ( $this->data['variant_urls'] as $link ) {
                            if ( stripos( $link['attributes'], 'selected' ) !== false ) {
                                $variantLabel = $link['text'];
                                break;
                            }
                        }
                        ?>
                        <h3 id="p-variants-label"><span><?php echo htmlspecialchars( $variantLabel ) ?></span><a href="#"></a></h3>

                        <div class="menu">
                            <ul>
                                <?php
                                foreach ( $this->data['variant_urls'] as $link ) {
                                    ?>
                                    <li<?php
                                    echo $link['attributes']
                                    ?>><a href="<?php
                                        echo htmlspecialchars( $link['href'] )
                                        ?>" lang="<?php
                                        echo htmlspecialchars( $link['lang'] )
                                        ?>" hreflang="<?php
                                        echo htmlspecialchars( $link['hreflang'] )
                                        ?>" <?php
                                        echo $link['key']
                                        ?>><?php
                                            echo htmlspecialchars( $link['text'] )
                                            ?></a></li>
                                <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <?php
                    break;
                case 'VIEWS':
                    ?>
                    <div id="p-views" role="navigation" class="wikiminiTabs<?php
                    if ( count( $this->data['view_urls'] ) == 0 ) {
                        echo ' emptyPortlet';
                    }
                    ?>" aria-labelledby="p-views-label">
                        <h3 id="p-views-label"><?php $this->msg( 'views' ) ?></h3>
                        <ul<?php
                        $this->html( 'userlangattributes' )
                        ?>>
                            <?php
                            foreach ( $this->data['view_urls'] as $link ) {
                                ?>
                                <li<?php
                                echo $link['attributes']
                                ?>><span><a href="<?php
                                        echo htmlspecialchars( $link['href'] )
                                        ?>" <?php
                                        echo $link['key'];
                                        if ( isset ( $link['rel'] ) ) {
                                            echo ' rel="' . htmlspecialchars( $link['rel'] ) . '"';
                                        }
                                        ?>><?php
                                            // $link['text'] can be undefined - bug 27764
                                            if ( array_key_exists( 'text', $link ) ) {
                                                echo array_key_exists( 'img', $link )
                                                    ? '<img src="' . $link['img'] . '" alt="' . $link['text'] . '" />'
                                                    : htmlspecialchars( $link['text'] );
                                            }
                                            ?></a></span></li>
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                    break;
                case 'ACTIONS':
                    ?>
                    <div id="p-cactions" role="navigation" class="wikiminiMenu<?php
                    if ( count( $this->data['action_urls'] ) == 0 ) {
                        echo ' emptyPortlet';
                    }
                    ?>" aria-labelledby="p-cactions-label">
                        <h3 id="p-cactions-label"><span><?php
                            $this->msg( 'wikimini-more-actions' )
                        ?></span><a href="#"></a></h3>

                        <div class="menu">
                            <ul<?php $this->html( 'userlangattributes' ) ?>>
                                <?php
                                foreach ( $this->data['action_urls'] as $link ) {
                                    ?>
                                    <li<?php
                                    echo $link['attributes']
                                    ?>>
                                        <a href="<?php
                                        echo htmlspecialchars( $link['href'] )
                                        ?>" <?php
                                        echo $link['key'] ?>><?php echo htmlspecialchars( $link['text'] )
                                            ?></a>
                                    </li>
                                <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <?php
                    break;
                case 'PERSONAL':
                    ?>
                    <div id="p-personal" role="navigation" class="<?php
                    if ( count( $this->data['personal_urls'] ) == 0 ) {
                        echo ' emptyPortlet';
                    }
                    ?>" aria-labelledby="p-personal-label">
                        <h3 id="p-personal-label"><?php $this->msg( 'personaltools' ) ?></h3>
                        <ul<?php $this->html( 'userlangattributes' ) ?>>
                            <?php
                            $personalTools = $this->getPersonalTools();
                            foreach ( $personalTools as $key => $item ) {
                                echo $this->makeListItem( $key, $item );
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                    break;
                case 'SEARCH':
                    ?>
                    <div id="p-search" role="search">
                        <h3<?php $this->html( 'userlangattributes' ) ?>>
                            <label for="searchInput"><?php $this->msg( 'search' ) ?></label>
                        </h3>

                        <form action="<?php $this->text( 'wgScript' ) ?>" id="searchform">
                            <?php
                            if ( $this->config->get( 'WikiminiUseSimpleSearch' ) ) {
                            ?>
                            <div id="simpleSearch">
                                <?php
                            } else {
                            ?>
                                <div>
                                    <?php
                            }
                            ?>
                            <?php
                            echo $this->makeSearchInput( array( 'id' => 'searchInput' ) );
                            echo Html::hidden( 'title', $this->get( 'searchtitle' ) );
                            // We construct two buttons (for 'go' and 'fulltext' search modes),
                            // but only one will be visible and actionable at a time (they are
                            // overlaid on top of each other in CSS).
                            // * Browsers will use the 'fulltext' one by default (as it's the
                            //   first in tree-order), which is desirable when they are unable
                            //   to show search suggestions (either due to being broken or
                            //   having JavaScript turned off).
                            // * The mediawiki.searchSuggest module, after doing tests for the
                            //   broken browsers, removes the 'fulltext' button and handles
                            //   'fulltext' search itself; this will reveal the 'go' button and
                            //   cause it to be used.
                            echo $this->makeSearchButton(
                                'fulltext',
                                array( 'id' => 'mw-searchButton', 'class' => 'searchButton mw-fallbackSearchButton' )
                            );
                            echo $this->makeSearchButton(
                                'go',
                                array( 'id' => 'searchButton', 'class' => 'searchButton' )
                            );
                            ?>
                                </div>
                        </form>
                    </div>
                    <?php

                    break;
            }
        }

        ?></div><?php
    }
    private function tooltipAndAccesskey($string) {
    return $string;
}

}
