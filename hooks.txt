Hooks provided by the Wikimini skin.

'SkinWikiminiStyleModules': Called when defining the list of module styles to be
loaded by the Wikimini skin.
$skin: SkinWikimini object
&$styles: Array of module names whose style will be loaded for the skin
