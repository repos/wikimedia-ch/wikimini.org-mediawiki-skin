These are historical credits to the project.
_________________________________________

[33mcommit 06371bbf20174383ca9f1ccd0d0e0e9363c775e9[m
Merge: c47c48f b9bc735
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 27 14:47:07 2015 +0200

    Merge branch 'master' of http://mcp.nixit.ch/wikimini/wikimini-skin

[33mcommit c47c48fef4248bd2d7ab4dd99308bf091b9ab7b1[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 27 14:46:34 2015 +0200

    Issue 42

[33mcommit b9bc735316b01e757735634ed3eaa936a918010d[m
Merge: 4802df9 bbc8ada
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Mon Jul 27 14:45:51 2015 +0200

    Merge branches

[33mcommit 4802df9f974273c32802d1b18f7064661328b0ad[m
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Mon Jul 27 14:44:56 2015 +0200

    Add SelectCategory arrow image

[33mcommit bbc8ada5837111ed394187f198c778091f46be8d[m
Merge: 76807c0 9e7e2b9
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 27 13:59:14 2015 +0200

    Merge branch 'master' of http://mcp.nixit.ch/wikimini/wikimini-skin

[33mcommit 76807c0384337f222298f46cb6d94c54015c0138[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 27 13:58:53 2015 +0200

    Bad files removed

[33mcommit 43d38b8192e9f2beb51df923944cdce6d0de158b[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 27 13:57:53 2015 +0200

    Blocking issues

[33mcommit 9e7e2b903441158fdacbbf798881a19ef3e1c7f3[m
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Mon Jul 27 13:53:31 2015 +0200

    Add css rules for the recent changes page

[33mcommit a5238d25880bb6b9881286555747c90f0df995ba[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Fri Jul 24 09:14:56 2015 +0200

    Design changes

[33mcommit 1c802fadc2db554d5b97902f6b4c4e852e8d80a3[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Thu Jul 23 16:00:17 2015 +0200

    Issue 33

[33mcommit fb47d8bf063ef080c659b602b1db9b306f76ec83[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Thu Jul 23 15:35:57 2015 +0200

    Same display conditions between optionbox and mobile-optionbox

[33mcommit d516bb075fd77cb9848c4fa7d0ea5d4b82e8e977[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Thu Jul 23 15:32:10 2015 +0200

    CSS id to class (multiple)

[33mcommit c40eac3c11270a16bc1f2409fc93b8dfabd1db6b[m
Merge: 8e14144 e6f7ee4
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Thu Jul 23 15:24:41 2015 +0200

    Merge branch 'master' of http://mcp.nixit.ch/wikimini/wikimini-skin

[33mcommit 8e1414413df695496441de3e2ac35b429d98be6c[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Thu Jul 23 15:12:16 2015 +0200

    issues 31, 30, 32

[33mcommit e6f7ee4188b6394bd8e9986c7a5a1e6ce74e2495[m
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Thu Jul 23 13:23:16 2015 +0200

    Add siteSub styling to wikimini.css

[33mcommit c9fcde58ada8c621f5823f97793bf813809abb32[m
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Thu Jul 23 13:13:15 2015 +0200

    Correct URL for icon-ca-talk in css

[33mcommit 8eee06d614eccdb78193c9df33fbc96fdf1c8043[m
Merge: a81db94 dfb41fd
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Wed Jul 22 15:17:25 2015 +0200

    Merge cedric and owen mods for tooltip

[33mcommit a81db94ce0be171be19dc4fd5eadfe88d0e3bc93[m
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Wed Jul 22 15:15:20 2015 +0200

    Change tooltip text and made them translatable in i18n folder

[33mcommit dfb41fd4c653109ed26dde423e64313dc044def0[m
Merge: 179901c ce8b10a
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 22 15:05:40 2015 +0200

    Merge branch 'master' of http://mcp.nixit.ch/wikimini/wikimini-skin

[33mcommit 179901c6fcb54dfc9b64209078eec0e951ae5bed[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 22 15:05:07 2015 +0200

    issue 30

[33mcommit ce8b10afc7e2d14bef9389042843fcd76985e6e0[m
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Wed Jul 22 14:43:52 2015 +0200

    Change modify link

[33mcommit fde4f57246220ee40156743ba78085fa55864e91[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 22 14:36:09 2015 +0200

    Issue #30

[33mcommit 78bf201c482e0aae61f5ef27363a16250003b9f2[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 22 13:40:36 2015 +0200

    issue 29 - CSS

[33mcommit a131f4f943037e6367b0e16c59b689fb6bf6ddc0[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 22 11:28:16 2015 +0200

    issue 14

[33mcommit fb86d72ec16d21707cb4fb7765dd1a0828022240[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 22 11:16:34 2015 +0200

    issue 28

[33mcommit e2590b0ebf42c2fdf6b0abc9fc45b8b90be42345[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 22 10:58:27 2015 +0200

    issue 3

[33mcommit 834edcfa446605d017d5f94dc4227956c15dea73[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 22 10:35:45 2015 +0200

    issue 26

[33mcommit 848c65e741274c2b935732a47c7a2a55b8ac063a[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 22 09:34:58 2015 +0200

    issue 24

[33mcommit 0d93c0a7543bc686ae830260d682991a6773c34a[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 22 09:02:48 2015 +0200

    "Full width icon", issue 23

[33mcommit dc33f396fef9897a54beab9b1217caf7426fb4bf[m
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Mon Jul 20 16:30:55 2015 +0200

    Change css as asked, print, sitesub spacing, etc.

[33mcommit 875995407bdea526c2c74f655daf9cf50d3e28d2[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 20 15:34:07 2015 +0200

    Index page changes (list instead of table, general formatting)

[33mcommit 1ba36b5784797ca350e661c7d7d3bf9948206df6[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 20 14:39:15 2015 +0200

    Issues -  bug fixes

[33mcommit 1beef61ea4f5bea4d54d0f5478a40b66bf88d000[m
Merge: 57a1ca7 2ded28a
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 20 14:38:41 2015 +0200

    Merge branch 'master' of http://mcp.nixit.ch/wikimini/wikimini-skin

[33mcommit 2ded28adb58d8c0aff67cc770e24a7c5764b9ad8[m
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Mon Jul 20 12:57:19 2015 +0200

    Add inline elements from fr.wikimini.org (Common and Wikimini .js and .css)

[33mcommit 57a1ca7466ab621e5ee01744dc2ddecea0d51b82[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Sat Jul 18 08:31:25 2015 +0200

    Bug fixes

[33mcommit 5e3ebd0555ef5049e74215063919f21f3e5d846a[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Thu Jul 9 16:30:08 2015 +0200

    Live changes

[33mcommit a00fc5dc93525d27287641e8c3e7cd873d4a5131[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Wed Jul 8 09:14:47 2015 +0200

    Toolbox design and pictures

[33mcommit 650503b69210a341c4de6c4eb92772b0529860fe[m
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Tue Jul 7 13:34:23 2015 +0200

    Change position of site-notice to be below banner

[33mcommit 1627a988c7ae3ee314be42f979be53ebea21fb34[m
Author: Owen McGill <owen.mcgill@nixit.ch>
Date:   Tue Jul 7 12:34:17 2015 +0200

    Add an include for picture of the day in WikiminiTemplate.php

[33mcommit 06af36b5ba2f6e300a97a835a04cc6ca4389e056[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Tue Jul 7 07:48:55 2015 +0200

    Responsive columns width adjustments

[33mcommit caa72959e6c9db485ba67a9e8dd1a212e789fb5b[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Tue Jul 7 07:27:14 2015 +0200

    Wikimini translations (fr)

[33mcommit 7dea18af1149c015137e4d59a87d80a5fece00a1[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 6 22:05:05 2015 +0200

    images from old skin

[33mcommit 24201a279669aa649fc5c0c47b956eaff5957208[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 6 21:37:18 2015 +0200

    Toolbox integration

[33mcommit cf2f3b6852fbef176b4be3d0f17bc4f981488d68[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Mon Jul 6 10:14:17 2015 +0200

    New resources - Searchbox

[33mcommit 64db9e0a6c35ae4a25fefe9bd51d94cf4a187fea[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Sat Jul 4 08:07:27 2015 +0200

    Columns test

[33mcommit ea78c82e2c2b2327da8ea1018a58e6b653e97adf[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Sat Jul 4 07:53:17 2015 +0200

    wikimini css created. Responsive menu

[33mcommit 28f16d577958b0d83a6bd799bef16cb9d9d1a0c8[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Sat Jul 4 07:33:37 2015 +0200

    CSS + model corrections

[33mcommit 9b0628d812cafb6bcf9f14a57919af1f31b8f56c[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Sat Jul 4 07:32:45 2015 +0200

    Templating removed. Header created

[33mcommit 5eb12f7f81e028a141a0b10c9f86172c16ad60c1[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Sat Jul 4 06:51:07 2015 +0200

    creating a file reference

[33mcommit 2919333120885cf6737e4ab4914d1f7baa91dde3[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Fri Jul 3 14:36:52 2015 +0200

    skin cleaned. Resources reorganized. Bootstrap and wikimini header integration

[33mcommit c48b2b46deba4f73e4677451f89cd2226563bfc8[m
Author: Cédric Daucourt <cedric.daucourt@nixit.ch>
Date:   Fri Jul 3 06:15:16 2015 +0200

    initial import
