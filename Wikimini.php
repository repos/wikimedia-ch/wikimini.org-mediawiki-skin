<?php
if ( !defined( 'MEDIAWIKI' ) ) {
	die( 'This is an extension to the MediaWiki package and cannot be run standalone.' );
}

if ( function_exists( 'wfLoadSkin' ) ) {
	wfLoadSkin( 'Wikimini' );

	// Keep i18n globals so mergeMessageFileList.php doesn't break
	$wgMessagesDirs['Wikimini'] = __DIR__ . '/i18n';

// 	$wgAutoloadClasses['SkinWikimini'] = __DIR__ . '/SkinWikimini.php';

	/* wfWarn(
		'Deprecated PHP entry point used for Wikimini skin. Please use wfLoadSkin instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	); */

	return true;
} else {
	die( 'This version of the Wikimini skin requires MediaWiki 1.25+' );
}
